import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './prod.html';

Template.prod.helpers({
// getClass(){
//   var t= template.instance();
//    var editible = t.dataDict.get("editible");
//    console.log(editible);
//    return editible;
// }
});

Template.prod.events({
  'click #delete'() {
    Meteor.call('prods.remove', this._id, function(err,res){
      if(err){
        toastr.error(err, "Error");
      }else {
          toastr.success("Product Deleted","Success");
      }
    });

  },
  'click #edit'(event,t) {
    event.preventDefault();
    const target = event.target;
    var name=target.prod_name.value;
    var count=target.prod_count.value;
    var prodId= this._id;
    console.log(name,count,prodId);
    t.dataDict.set("editible",prodId);
    Meteor.call('prods.update',prodId,name,count,function(err,res){
      if(err){
        toastr.error(err, "Error");
      }
      else{
        toastr.success("Product updated","Error");
      }
    });
  },
});

Template.prod.onCreated(function(){
  var self= this;
Meteor.subscribe('prods');
  self.dataDict= new ReactiveDict();
 self.dataDict.set("editible","");
 self.dataDict.set("url","");
});
