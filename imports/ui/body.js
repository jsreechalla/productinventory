import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Products } from '../api/products.js';

import './prod.js';
import './body.html';

Template.body.onCreated(function bodyOnCreated() {
  this.dataDict = new ReactiveDict();
  Meteor.subscribe('prods');
});

Template.body.helpers({
  prods() {
    return Products.find({}).fetch();
  }
});

Template.body.events({
  'submit #newItem':function(event,t) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;
    const count =target.count.value
    const url=t.dataDict.get("url");
    // Insert a prod into the collection
    Meteor.call('prods.insert', text,count,function (err,res) {
      if(err){
        toastr.error(err,"Error");
      }else{
        toastr.success("New Product Inserted", "Success");
      }
    });

    // Clear form
    target.text.value = '';
    target.count.value ='';
  },
  'change .upload':function(e,t){
      var id=e.target.id;
      var preview=document.querySelector("img");
      var file= e.target.files[0];
      var reader =new FileReader();
      console.log(file,e.target);
      var url="";
      if(preview.id===id){
        reader.onload =function(event){
        preview.src=reader.result;
        t.dataDict.set("url",reader.result);
        }
      reader.readAsDataURL(file)
       console.log(reader.result);
      }             },

});
