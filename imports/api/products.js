import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Products = new Mongo.Collection('Products');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish prods that are public or belong to the current user
  Meteor.publish('prods', function() {
    return Products.find();
  });
}

Meteor.methods({
  'prods.insert'(text,count,file) {
    check(text, String);
    check(count,String);
    Products.insert({
      name:text,
      number:count,
      url:file,
      createdAt: new Date(),
    });
  },
  'prods.remove'(prodId) {
    check(prodId, String);
    const prod = Products.findOne(prodId);
    Products.remove(prodId);
  },
  'prods.update'(prodId,name,count){
    check(prodId,String);
    check(name,String);
    check(count,String);
    Products.update({"_id":prodId},{$set:{"name":name,"count":count}});
  }
});
